/*
 * AppDelegate.swift
 * NSPropertyDescription Hash Bug iOS
 *
 * Created by François Lamboley on 28/06/2020.
 * Copyright © 2020 frizlab. All rights reserved.
 */

import UIKit



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		checkBug(modelURL: Bundle.main.url(forResource: "BigModel", withExtension: "momd")!)
		return true
	}
	
}
