/*
 * main.swift
 * NSPropertyDescription Hash Bug
 *
 * Created by François Lamboley on 03/06/2017.
 * Copyright © 2017 frizlab. All rights reserved.
 */

import Foundation



checkBug(modelURL: URL(fileURLWithPath: CommandLine.arguments[0]).deletingLastPathComponent().appendingPathComponent("BigModel.momd"))
