/*
 * CheckBug.swift
 * NSPropertyDescription Hash Bug
 *
 * Created by François Lamboley on 03/06/2017.
 * Copyright © 2017 frizlab. All rights reserved.
 */

import CoreData
import Foundation
import ObjectiveC.runtime



func checkBug(modelURL: URL) {
	let model = NSManagedObjectModel(contentsOf: modelURL)!
	
	let childProperty = model.entitiesByName["FLHPCrossingFeedItem"]!.propertiesByName["zzRID"]!
	print("Child  Property Type: \(object_getClass(childProperty)!)")
	print("Child  Property Hash Value: \(childProperty.hash)")
	
	let parentProperty = model.entitiesByName["FLHPCrossing"]!.propertiesByName["zzRID"]!
	print("Parent Property Type: \(object_getClass(parentProperty)!)")
	print("Parent Property Hash Value: \(parentProperty.hash)")
	
	print(parentProperty == childProperty)
	
	if parentProperty == childProperty && parentProperty.hash != childProperty.hash {
		print("BUG IN CORE DATA! The two properties are equal, but the hashes are not.")
	}
}
